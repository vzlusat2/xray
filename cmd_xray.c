#include <console/cmddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>

#include <csp/csp_endian.h>

#include "xray.h"

#define EXP_XRAY_CSP_ADDR      (NODE_XRAY)
#define EXP_XRAY_TIMEOUT       1500

/* -----  DK ----------------------------- */
typedef enum
{
  DKC_STORE = 0,       //Store a chunk of data.
  DKC_STORE_ACK = 1,   //Store a chunk of data and reply result
  DKC_GET_RAW = 2,     //Retrieve only a chunk of data
  DKC_GET_RICH = 3,    //Retrieve a data with all adational info
  DKC_GET_RANGE = 4,   //Retrive a range of chunks
  DKC_GET_NUM = 5,     //Retrive sepecified number of chunks
  DKC_FIND = 6,        //Retrive a chunk ID with certain store time
  DKC_INFO = 10,       //Information about one storage
  DKC_LIST = 11,       //List of all storages information
  DKC_INFO_RICH = 12,  //Rich information about one storage
  DKC_CREATE = 13,     //Create new storage
  DKC_WIPE = 14,       //Delete all data from storage
  DKC_CLEAN = 15,      //Clean chunks from storage
  DKC_STORE_FULL = 16, //Store a chunk with defined time and flags
  DKC_CREATE_FOR = 17, //Create new storage for host
  DKC_STRIP = 18,      //Delete 1/2 of storage data (oldest)
  DKC_DELETE = 19,     //Delete a storage with all it's data
  DKC_STATS = 20,      //Statistics about space and storages
  DKC_MAINTENANCE = 21,//Repair storages and actual remove cleaned chunks
  DKC_INIT = 22,       //Re-load cache from files
} DK_COMMAND_ENUM;

typedef struct __attribute__((packed))
{ //DKC_LIST, DKC_STATS, DKC_BCK_*
  uint8_t cmd;        //DK_COMMAND_ENUM
} dk_msg_t;

typedef struct __attribute__((packed))
{ //DKC_INFO, DKC_INFO_RICH, DKC_REMOVE, DKC_MAINTENANCE
  dk_msg_t parent;
  uint8_t host;       //storage identification
  uint8_t port;       //storage sub-identification
} dk_msg_storage_t;

typedef struct __attribute__((packed))
{ //DKC_STORE_ACK, DKC_CREATE, DKC_CREATE_FOR, DKC_REMOVE, DKC_CLEAN, DKC_STORE_FULL, DKC_MAINTENANCE
  uint8_t host;    //storage identification
  uint8_t port;    //storage sub-identification
  int reply;       //internal error number or 0 when success
  uint16_t seq;    //sequence number of reply. lower 2B od last_chunk_id for DKC_STORE_ACK and DKC_STORE_FULL
} dk_reply_common_t;

typedef struct __attribute__((packed))
{ //DKC_GET_RAW, DKC_GET_RICH
  dk_msg_t parent;
  uint8_t host;       //storage identification
  uint8_t port;       //storage sub-identification
  uint32_t chunk;     //ID of data chunk
} dk_msg_get_t;

typedef struct __attribute__((packed))
{ //DKC_INFO, DKC_LIST
  uint8_t host;        //storage identification
  uint8_t port;        //storage sub-identification
  uint8_t errors;      //number of read / write / open errors
  uint32_t newest_chunk_id;
  uint32_t write_tim;  //since 01.01.2000 00:00:00 UTC
  uint32_t oldest_chunk_id;
} dk_reply_info_t;

typedef struct __attribute__((packed))
{ //DKC_STORE_ACK
  dk_msg_t parent;
  uint8_t host;       //storage identification
  uint8_t port;       //identify where data to be stored (sub_id)
                      //bits 6-0 is storage number from 0 to 128.
                      //bit 7 identify first data chunk - configurations
  uint8_t data[0];    //array of data
} dk_msg_store_ack_t;

#define NUMBER_DK_REQ_TRIALS 3

/* --------------------------------------- */

uint32_t getLastChunkId(uint8_t port) {

  uint8_t buf[sizeof(dk_msg_storage_t)];
  dk_msg_storage_t * message = (dk_msg_storage_t *)buf;
  
  message->host = NODE_XRAY;
  message->port = STORAGE_METADATA_ID;
  message->parent.cmd = DKC_INFO;

  uint32_t chunksNum = 0;

  dk_reply_info_t reply;
  
  usleep(XRAY_DK_SLEEP_TIME_MS*1000);

  int ierr;
  for (size_t i = 0; i<NUMBER_DK_REQ_TRIALS; i++) {
    ierr = 1;
    if(csp_transaction(CSP_PRIO_NORM, NODE_OBC, PORT_DK, 1000, buf, sizeof(dk_msg_storage_t), &reply, sizeof(dk_reply_info_t))) {
      chunksNum = csp_ntoh32(reply.newest_chunk_id);
      ierr = 0;
      break;
    }
    usleep(XRAY_DK_SLEEP_TIME_MS*1000);
  }

  usleep(XRAY_DK_SLEEP_TIME_MS*1000);

  return chunksNum;
}

static int xray_print_reply( uint8_t ack )
{
  struct ack_list_t {
    uint8_t ack;
    const char *name;
  } ack_list[] = {
    { XRAY_OK,                                             "XRAY_OK" },
    { XRAY_CAMERA_CONFIG_DEVICE_ERROR,                     "XRAY_CAMERA_CONFIG_DEVICE_ERROR" },
    { XRAY_CAMERA_DK_CONF_CHUNK_ERROR,                     "XRAY_CAMERA_DK_CONF_CHUNK_ERROR" },
    { XRAY_CAMERA_UNSPECIFIED_ERROR,                       "XRAY_CAMERA_UNSPECIFIED_ERROR" },
  };

  for (int i = 0; i < (sizeof(ack_list)/sizeof(struct ack_list_t)); i++) {
    if (ack == ack_list[i].ack) {
      console_printf("%s\r\n", ack_list[i].name);
      return ack == XRAY_OK ? CONSOLE_OK : CONSOLE_ERROR;
    }
  }
  console_printf("ack not found!\r\n");
  return CONSOLE_ERR_INVALID_ARG;
}


int xray_cmd_set_roi(console_ctx_t *ctx, cmd_signature_t *reg)
{
  char buffer[XRAY_REPLY_BUF_SZ];
  
  static struct {
	struct arg_int *id;
        struct arg_int *row;
        struct arg_int *col;
        struct arg_int *height;
        struct arg_int *width;
        struct arg_end *end;
  } args;

  if (reg) {
    args.id     = arg_int0(NULL, NULL, "<int>", "ROI id (default 0)");
    args.row    = arg_int0(NULL, NULL, "<int>", "ROI row (default 0)");
    args.col    = arg_int0(NULL, NULL, "<int>", "ROI col (default 0)");
    args.height = arg_int0(NULL, NULL, "<int>", "ROI height (default 256)");
    args.width  = arg_int0(NULL, NULL, "<int>", "ROI width (default 256)");
    args.end    = arg_end(1);

    reg->argtable = &args;
    reg->command = "x sr";
    reg->help = "X-Ray Camera Set ROI (id, row, col, height, width)";
    return CONSOLE_OK;
  }

  int id     = args.id->count ? args.id->ival[0] : 0;
  int row    = args.row->count ? args.row->ival[0] : 0;
  int col    = args.col->count ? args.col->ival[0] : 0;
  int height = args.height->count ? args.height->ival[0] : 256;
  int width  = args.width->count ? args.width->ival[0] : 256;
  
  xray_cmd_setroi_t xray_cmd;
  int rv;

  xray_cmd.parent.cmd = XRAY_DETECTOR_SET_ROI;
  xray_cmd.id = id;
  xray_cmd.row = row;
  xray_cmd.col = col;
  xray_cmd.height = height;
  xray_cmd.width = width;

  rv = csp_transaction(CSP_PRIO_NORM,
                       NODE_XRAY, XRAY_PORT_DIRECT,
                       2000, &xray_cmd,
                       sizeof(xray_cmd_setroi_t),
                       &buffer,
                       -1);

  if (!rv) {
    console_printf("X-Ray Camera set ROI error!\r\n");
    return CONSOLE_ERROR;
  } else {
    console_printf("%s\r\n", buffer);
    return CONSOLE_OK;
  }
}

int xray_cmd_set_parameters(console_ctx_t *ctx, cmd_signature_t *reg)
{
  char buffer[XRAY_REPLY_BUF_SZ];
  
  static struct {
        struct arg_int *mode;
        struct arg_dbl *threshold;
        struct arg_int *frameTime;
        struct arg_int *nFrames;
        struct arg_int *filtering;
        struct arg_int *outputform;
        struct arg_int *scanMode;
        struct arg_int *countThreshold;
        struct arg_int *countROI;
        struct arg_int *en_hist_limit;
        struct arg_end *end;
  } args;

  if (reg) {
    args.mode            = arg_int0(NULL, NULL, "<int>", "mode (default 0 = MPX)");
    args.threshold       = arg_dbl0(NULL, NULL, "<dbl>", "threshold (default 5.0 keV)");
    args.frameTime       = arg_int0(NULL, NULL, "<int>", "exposure (default 100 ms)");
    args.nFrames         = arg_int0(NULL, NULL, "<int>", "nb. frames (default 1)");
    args.filtering       = arg_int0(NULL, NULL, "<int>", "filtering (default 1 = YES)");
    args.outputform      = arg_int0(NULL, NULL, "<int>", "outputform (default 63)");
    args.scanMode        = arg_int0(NULL, NULL, "<int>", "scan mode (default 0 = NO)");
    args.countThreshold  = arg_int0(NULL, NULL, "<int>", "count threshold (default 256)");
    args.countROI        = arg_int0(NULL, NULL, "<int>", "count ROI (default 0)");
    args.en_hist_limit   = arg_int0(NULL, NULL, "<int>", "energy histogram range (default 256)");

    args.end    = arg_end(1);

    reg->argtable = &args;
    reg->command = "x sp";
    reg->help = "X-Ray set parameters (mode, threshold, exposure, frames, filtering, outputform, scan, count-threshold, count-roi, en-hist-limit)";
    return CONSOLE_OK;
  }

  int mode           = args.mode->count ? args.mode->ival[0] : 0;
  double threshold   = args.threshold->count ? args.threshold->dval[0] : 5.0;
  int frameTime      = args.frameTime->count ? args.frameTime->ival[0] : 100;
  int nFrames        = args.nFrames->count ? args.nFrames->ival[0] : 1;
  int filtering      = args.filtering->count ? args.filtering->ival[0] : 1;
  int outputform     = args.outputform->count ? args.outputform->ival[0] : 63;
  int scanMode       = args.scanMode->count ? args.scanMode->ival[0] : 0;
  int countThreshold = args.countThreshold->count ? args.countThreshold->ival[0] : 256;
  int countROI       = args.countROI->count ? args.countROI->ival[0] : 0;
  int en_hist_limit  = args.en_hist_limit->count ? args.en_hist_limit->ival[0] : 256;
  
  xray_cmd_setparam_t xray_cmd;
  int rv;
  
  xray_cmd.parent.cmd = XRAY_DETECTOR_SET_PARAMETERS;
  xray_cmd.mode = mode;
  xray_cmd.threshold = threshold;
  xray_cmd.frameTime = frameTime;
  xray_cmd.nFrames = nFrames;
  xray_cmd.filtering = filtering;
  xray_cmd.outputform = outputform;
  xray_cmd.scanMode = scanMode;
  xray_cmd.countThreshold = countThreshold;
  xray_cmd.countROI = countROI;
  xray_cmd.en_hist_limit = en_hist_limit;

  rv = csp_transaction(CSP_PRIO_NORM,
                       NODE_XRAY, XRAY_PORT_DIRECT,
                       2000, &xray_cmd,
                       sizeof(xray_cmd_setparam_t),
                       &buffer,
                       -1);

  if (!rv) {
    console_printf("X-Ray Camera set Parameters error!\r\n");
    return CONSOLE_ERROR;
  } else {
    console_printf("%s\r\n", buffer);
    return CONSOLE_OK;
  }
}

int xray_cmd_set_count(console_ctx_t *ctx, cmd_signature_t *reg)
{
  char buffer[XRAY_REPLY_BUF_SZ];
  
  static struct {
	      struct arg_int *treshold;
        struct arg_int *id;
        struct arg_end *end;
  } args;

  if (reg) {
    args.treshold  = arg_int0(NULL, NULL, "<int>", "Treshold (default 256)");
    args.id        = arg_int0(NULL, NULL, "<int>", "Id (default 0)");
    args.end       = arg_end(1);

    reg->argtable = &args;
    reg->command = "x spc";
    reg->help = "X-Ray set Count treshold (treshold, id)";
    return CONSOLE_OK;
  }

  int treshold = args.treshold->count ? args.treshold->ival[0] : 256;
  int id       = args.id->count ? args.id->ival[0] : 0;
  
  xray_cmd_setcount_t xray_cmd;
  int rv;

  xray_cmd.parent.cmd = XRAY_SET_COUNT_TRESHOLD;
  xray_cmd.treshold = treshold;
  xray_cmd.id = id;

  rv = csp_transaction(CSP_PRIO_NORM,
                       NODE_XRAY, XRAY_PORT_DIRECT,
                       2000, &xray_cmd,
                       sizeof(xray_cmd_setcount_t),
                       &buffer,
                       -1);

  if (!rv) {
    console_printf("X-Ray Camera set Count error!\r\n");
    return CONSOLE_ERROR;
  } else {
    console_printf("%s\r\n", buffer);
    return CONSOLE_OK;
  }
}

int xray_cmd_set_autoexec(console_ctx_t *ctx, cmd_signature_t *reg)
{
  char buffer[XRAY_REPLY_BUF_SZ];

  static struct {
    struct arg_int *autoexec;
    struct arg_int *poweroff;
    struct arg_end *end;
  } args;

  if (reg) {
    args.autoexec  = arg_int0(NULL, NULL, "<int>", "Autoexec (default 0 .. OFF)");
    args.poweroff  = arg_int0(NULL, NULL, "<int>", "PowerOff (default 0 .. OFF)");
    args.end       = arg_end(1);

    reg->argtable = &args;
    reg->command = "x sax";
    reg->help = "X-Ray set auto-execution (autorun, poweroff)";
    return CONSOLE_OK;
  }

  int autoexec = args.autoexec->count ? args.autoexec->ival[0] : 0;
  int poweroff = args.poweroff->count ? args.poweroff->ival[0] : 0;

  xray_cmd_autoexec_t xray_cmd;
  int rv;

  xray_cmd.parent.cmd = XRAY_SET_AUTOEXEC;
  xray_cmd.autoexec = autoexec;
  xray_cmd.poweroff = poweroff;

  rv = csp_transaction(CSP_PRIO_NORM,
                       NODE_XRAY, XRAY_PORT_DIRECT,
                       2000, &xray_cmd,
                       sizeof(xray_cmd_autoexec_t),
                       &buffer,
                       -1);

  if (!rv) {
    console_printf("X-Ray Camera set auto-execution error!\r\n");
    return CONSOLE_ERROR;
  } else {
    console_printf("%s\r\n", buffer);
    return CONSOLE_OK;
  }
}



int xray_cmd_templ_no_arg(console_ctx_t *ctx, cmd_signature_t *reg,
			  char * command, char * help,
			  uint8_t port, xray_cmd_list_t cmd, char* err, int timeout)
{
  char buffer[XRAY_REPLY_BUF_SZ];
  
  static struct {
    struct arg_end *end;
  } args;

 if (reg) {
    args.end = arg_end(1);

    reg->argtable = &args;
    reg->command = command;
    reg->help = help;
    return CONSOLE_OK;
  }

  xray_cmd_t xray_cmd;
  int rv;

  xray_cmd.cmd = cmd;

  rv = csp_transaction(CSP_PRIO_NORM,
                       NODE_XRAY, port,
                       timeout, &xray_cmd,
                       sizeof(xray_cmd_t),
                       &buffer,
                       -1);

  if (!rv) {
    console_printf("%s\r\n", err);
    return CONSOLE_ERROR;
  } else {
    console_printf("%s\r\n", buffer);
    return CONSOLE_OK;
  }
}

int xray_cmd_templ_single_int(console_ctx_t *ctx, cmd_signature_t *reg,
			      char * command, char * help,
			      char * argname, char * arghint, int default_val,
			      uint8_t port, xray_cmd_list_t cmd, char* err, int timeout)
{
  char buffer[XRAY_REPLY_BUF_SZ];

  static struct {
    struct arg_int *iarg;
    struct arg_end *end;
  } args;

  if (reg) {
    args.iarg = arg_int0(NULL, NULL, argname, arghint);
    args.end = arg_end(1);

    reg->argtable = &args;
    reg->command = command;
    reg->help = help;
    return CONSOLE_OK;
  }

  int iarg = args.iarg->count ? args.iarg->ival[0] : default_val;
 
  xray_cmd_single_int_t xray_cmd;
  int rv;

  xray_cmd.parent.cmd = cmd;
  xray_cmd.arg0 = iarg;
  
  rv = csp_transaction(CSP_PRIO_NORM,
                       NODE_XRAY, port,
                       timeout, &xray_cmd,
                       sizeof(xray_cmd_single_int_t),
                       &buffer,
                       -1);

  if (!rv) {
    console_printf("%s\r\n", err);
    return CONSOLE_ERROR;
  } else {
    console_printf("%s\r\n", buffer);
    return CONSOLE_OK;
  }
}

int xray_cmd_templ_single_dbl(console_ctx_t *ctx, cmd_signature_t *reg,
			      char * command, char * help,
			      char * argname, char * arghint, double default_val,
			      uint8_t port, xray_cmd_list_t cmd, char* err, int timeout)
{
  char buffer[XRAY_REPLY_BUF_SZ];

  static struct {
    struct arg_dbl *darg;
    struct arg_end *end;
  } args;

  if (reg) {
    args.darg = arg_dbl0(NULL, NULL, argname, arghint);
    args.end = arg_end(1);

    reg->argtable = &args;
    reg->command = command;
    reg->help = help;
    return CONSOLE_OK;
  }

  double darg = args.darg->count ? args.darg->dval[0] : default_val;

  xray_cmd_single_dbl_t xray_cmd;
  int rv;

  xray_cmd.parent.cmd = cmd;
  xray_cmd.arg0 = darg;

  rv = csp_transaction(CSP_PRIO_NORM,
                       NODE_XRAY, port,
                       timeout, &xray_cmd,
                       sizeof(xray_cmd_single_dbl_t),
                       &buffer,
                       -1);
  if (!rv) {
    console_printf("%s\r\n", err);
    return CONSOLE_ERROR;
  } else {
    console_printf("%s\r\n", buffer);
    return CONSOLE_OK;
  }
}

int xray_cmd_get_metadata(console_ctx_t *ctx, cmd_signature_t *reg)
{
  char buffer[sizeof(experiment_metainfo_t)];

  static struct {
    struct arg_int *chunk;
    struct arg_end *end;
  } args;

 if (reg) {
   args.chunk = arg_int0(NULL, NULL, "<int>", "measurement nb (default 0 = last)");
   args.end = arg_end(1);
   
   reg->argtable = &args;
   reg->command = "x gm";
   reg->help = "Get image metadata [fetch DK]";
   return CONSOLE_OK;
 }

 uint32_t last_chunk_id = getLastChunkId(STORAGE_METADATA_ID);
  
 if (!last_chunk_id) {
   console_printf("%s\r\n", "No data");
   return CONSOLE_ERROR;
 }

 uint32_t chunk_id = args.chunk->count ? args.chunk->ival[0] : 0;

 if((chunk_id==0) || (chunk_id>last_chunk_id))
   chunk_id = last_chunk_id;
   
 dk_msg_get_t req;
 int rv;

 req.parent.cmd = DKC_GET_RAW;
 req.host = NODE_XRAY;
 req.port = STORAGE_METADATA_ID;
 req.chunk = csp_hton32(chunk_id);
 
 rv = csp_transaction(CSP_PRIO_NORM,
		      NODE_OBC, PORT_DK,
		      2000, &req,
		      sizeof(dk_msg_get_t),
		      &buffer,
		      sizeof(experiment_metainfo_t));

 if (!rv) {
   console_printf("%s\r\n", "error");
   return CONSOLE_ERROR;
 } else {
   experiment_metainfo_t * ei = (experiment_metainfo_t *) buffer; 
   console_printf("[RITE] start_nb: %u\r\n", ei->start_nb);
   console_printf("[MEAS] exp_nb: %u\r\n", ei->exp_nb);
   console_printf("[MEAS] frame_id: 0x%04x\r\n", ei->frame_id);
   time_t t = ei->tv_sec;
   struct tm tm = *localtime(&t);
   console_printf("[MEAS] time: %d-%02d-%02d %02d:%02d:%02d.%03u\r\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, ei->tv_nsec/1000000);
   console_printf("[DATA] outputForm: %u\r\n", ei->outputForm);
   console_printf("[DATA] firstChunkId: %u\r\n", ei->firstChunkId);
   console_printf("[DATA] lastChunkId: %u\r\n", ei->lastChunkId);
   console_printf("[DET] mode: %u\r\n", ei->mode);
   console_printf("[DET] scan mode, (count) threshold, roi: %u, %u, %u\r\n", ei->scanMode, ei->count_threshold, ei->count_roi);
   console_printf("[DET] frameTime: %g (sec)\r\n", ei->frameTime);
   console_printf("[DET] bias: %g (V)\r\n", ei->bias);
   console_printf("[DET] threshold: %g (keV)\r\n", ei->threshold);
   console_printf("[DET] nFrames: %u \r\n", ei->nFrames);
   console_printf("[PROC] filtering: %u \r\n", ei->filtering);
   console_printf("[PROC] (frame) min, max, (filtered) min, max: %10u %10u %10u %10u\r\n", ei->minValueOriginal, ei->maxValueOriginal, ei->minValueFiltered, ei->maxValueFiltered);
   console_printf("[PROC]    id  row  col  hgt  wdh  frame-evt   filt-evt  frame-sum   filt-sum\r\n");
   for(size_t iroi=0; iroi<DET_NROIS; iroi++)
     console_printf("[PROC] roi%ld: %4u %4u %4u %4u %10u %10u %10u %10u\r\n", iroi, ei->rois[iroi][0], ei->rois[iroi][1], ei->rois[iroi][2], ei->rois[iroi][3],
		    ei->frame_events[iroi], ei->frame_events_filtered[iroi], ei->roi_sum[iroi], ei->roi_sum_filtered[iroi]);
   console_printf("[PROC] en_hist_limit: %u \r\n", ei->en_hist_limit);   
   return CONSOLE_OK;
 }
}

int xray_cmd_get_status(console_ctx_t *ctx, cmd_signature_t *reg)
{
  char buffer[sizeof(xray_status_t)];

  static struct {
    struct arg_end *end;
  } args;

 if (reg) {
   args.end = arg_end(1);

   reg->argtable = &args;
   reg->command = "x gs";
   reg->help = "X-Ray get status";
   return CONSOLE_OK;
 }

 xray_cmd_t xray_cmd;
 int rv;

 xray_cmd.cmd = XRAY_GET_STATUS;
 
 rv = csp_transaction(CSP_PRIO_NORM,
		      NODE_XRAY, XRAY_PORT_DIRECT,
		      2000, &xray_cmd,
		      sizeof(xray_cmd_t),
		      &buffer,
		      -1);
 
  if (!rv) {
    console_printf("X-Ray Camera get status error!\r\n");
    return CONSOLE_ERROR;
  } else if(rv!=sizeof(xray_status_t)) {
    console_printf("%s\r\n", buffer);
    return CONSOLE_ERROR;
  } else {
    xray_status_t * status = (xray_status_t*) buffer;
    if(status->packetType != 'S') {
      console_printf("X-Ray Camera get status error (wrong packet)!\r\n");
      return CONSOLE_ERROR;
    }
    time_t t = status->tv_sec;
    struct tm tm = *localtime(&t);
    console_printf("[STATUS]     time: %d-%02d-%02d %02d:%02d:%02d.%03u\r\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, status->tv_nsec/1000000);
    console_printf("[STATUS] detector: %s\r\n", (status->status & 0x1) ? "idle" : "busy") ;
    console_printf("[STATUS]      i/o: %s\r\n", (status->status & 0x2) ? "idle" : "busy") ;
    return CONSOLE_OK;
  }
}

int xray_cmd_set_autoexec_dk(console_ctx_t *ctx, cmd_signature_t *reg)
{
  static struct {
    struct arg_int *autoexec;
    struct arg_int *poweroff;
    struct arg_end *end;
  } args;

  if (reg) {
    args.autoexec  = arg_int0(NULL, NULL, "<int>", "Autoexec (default 0 .. OFF)");
    args.poweroff  = arg_int0(NULL, NULL, "<int>", "PowerOff (default 0 .. OFF)");
    args.end       = arg_end(1);

    reg->argtable = &args;
    reg->command = "x saxdk";
    reg->help = "X-Ray set auto-execution (autorun, poweroff) [via DK]";
    return CONSOLE_OK;
  }

  int autoexec = args.autoexec->count ? args.autoexec->ival[0] : 0;
  int poweroff = args.poweroff->count ? args.poweroff->ival[0] : 0;

  dk_msg_get_t req;
  
  req.parent.cmd = DKC_GET_RAW;
  req.host = NODE_XRAY;
  req.port = STORAGE_METADATA_ID;
  req.chunk = csp_hton32(0);

  uint8_t buffer[sizeof(experiment_config_t)];

  int rv;
  
  rv = csp_transaction(CSP_PRIO_NORM,
		      NODE_OBC, PORT_DK,
		      2000, &req,
		      sizeof(dk_msg_get_t),
		      &buffer,
		      sizeof(experiment_config_t));
  
  if (!rv) {
    console_printf("X-Ray Camera set auto-execution error (get config)!\r\n");
    return CONSOLE_ERROR;
  }
  
  experiment_config_t * cfg = (experiment_config_t*) buffer;
  
  cfg->automeasure = autoexec;
  cfg->turnoff = poweroff;

  uint8_t buffer2[sizeof(dk_msg_store_ack_t)+sizeof(experiment_config_t)];
  dk_msg_store_ack_t * req2 = (dk_msg_store_ack_t *)buffer2;

  req2->parent.cmd = DKC_STORE_ACK;
  req2->port = STORAGE_METADATA_ID | 0x80;
  req2->host = NODE_XRAY;

  memcpy(req2->data, cfg, sizeof(experiment_config_t));

  uint8_t buffer3[sizeof(dk_reply_common_t)];

  rv = csp_transaction(CSP_PRIO_NORM,
		      NODE_OBC, PORT_DK,
		      2000, req2,
		      sizeof(dk_msg_store_ack_t)+sizeof(experiment_config_t),
		      &buffer3,
		      sizeof(dk_reply_common_t));

  if (!rv) {
    console_printf("X-Ray Camera set auto-execution error (save config)!\r\n");
    return CONSOLE_ERROR;
  }

  dk_reply_common_t * ack = (dk_reply_common_t*) buffer3;
  
  if (csp_ntoh32(ack->reply)) {
    console_printf("X-Ray Camera set auto-execution error (save config ack)!\r\n");
    return CONSOLE_ERROR;
  }
   else {
    return CONSOLE_OK;
  }
}

// request to create all storages in DK
int xray_cmd_create_storages(console_ctx_t *ctx, cmd_signature_t *reg)
{
  return xray_cmd_templ_no_arg(ctx, reg,
			       "x cs",
			       "Create DK storages",
			       XRAY_PORT_DIRECT,
			       XRAY_DK_CREATE_STORAGES,
			       "X-Ray creating storages error!",
			       20000);
}

int xray_cmd_usb_power(console_ctx_t *ctx, cmd_signature_t *reg)
{
  return xray_cmd_templ_single_int(ctx, reg,
                                   "x pwr",
                                   "X-Ray Turn on/off power for camera USB port",
                                   "<int>", "0 = OFF, 1 = ON", 1,
                                   XRAY_PORT_DIRECT,
                                   XRAY_USB_POWER,
                                   "X-Ray Camera set usb power error!",
                                   2000);
}

int xray_cmd_detector_init(console_ctx_t *ctx, cmd_signature_t *reg)
{
   return xray_cmd_templ_no_arg(ctx, reg,
                               "x in",
                               "X-Ray Initialize Timepix",
                               XRAY_PORT_DIRECT,
                               XRAY_DETECTOR_INIT,
                               "X-Ray Timepix initialization error!",
                               2000);
}

int xray_cmd_system_shutdown(console_ctx_t *ctx, cmd_signature_t *reg)
{
  return xray_cmd_templ_no_arg(ctx, reg,
                               "x sd",
                               "X-Ray Shut down Mars control computer",
                               XRAY_PORT_DIRECT,
                               XRAY_SYSTEM_SHUTDOWN,
                               "X-Ray Mars shutdown error!",
                               2000);
}

int xray_cmd_detector_set_acq_mode(console_ctx_t *ctx, cmd_signature_t *reg)
{
  return xray_cmd_templ_single_int(ctx, reg,
				   "x sm",
				   "X-Ray Camera set aquisition mode (0 = MPX, 1 = TOT, 2 = TPX)",
				   "<int>", "0 = MPX(counting), 1 = TOT(energy), 2 = TPX(timepix), default 0", 0,             
				   XRAY_PORT_DIRECT,
				   XRAY_DETECTOR_SET_ACQ_MODE,
				   "X-Ray Camera set acq mode error!",
				   2000);
}

int xray_cmd_detector_set_bias(console_ctx_t *ctx, cmd_signature_t *reg)
{
  return xray_cmd_templ_single_dbl(ctx, reg,
                                   "x sb",
                                   "X-Ray Camera set bias (V) (default 50.0)",
                                   "<real>", "Bias (V)", 50.0,
                                   XRAY_PORT_DIRECT,
                                   XRAY_DETECTOR_SET_BIAS,
                                   "X-Ray Camera set bias error!",
                                   2000);
}

int xray_cmd_detector_set_threshold(console_ctx_t *ctx, cmd_signature_t *reg)
{
  return xray_cmd_templ_single_dbl(ctx, reg,
                                   "x st",
                                   "X-Ray Camera set energy threshold (keV) (default 5.0)",
                                   "<real>", "Threshold (keV)", 5.0,
                                   XRAY_PORT_DIRECT,
                                   XRAY_DETECTOR_SET_THRESHOLD,
                                   "X-Ray Camera set threshold error!",
                                   2000);
}

int xray_cmd_detector_set_exposure(console_ctx_t *ctx, cmd_signature_t *reg)
{
  return xray_cmd_templ_single_int(ctx, reg,
                                   "x se",
                                   "X-Ray Camera set exposure time (ms) (default 100)",
                                   "<int>", "ExposureTime (ms)", 100,
                                   XRAY_PORT_DIRECT,
                                   XRAY_DETECTOR_SET_EXPOSURE,
                                   "X-Ray Camera set exposure time error!",
                                   2000);
}

int xray_cmd_detector_set_period(console_ctx_t *ctx, cmd_signature_t *reg)
{
  return xray_cmd_templ_single_int(ctx, reg,
                                   "x spe",
                                   "X-Ray Camera set frame period (ms) (default 0)",
                                   "<int>", "FramePeriod (ms)", 0,
                                   XRAY_PORT_DIRECT,
                                   XRAY_DETECTOR_SET_PERIOD,
                                   "X-Ray Camera set frame period error!",
                                   2000);
}

int xray_cmd_detector_set_nframes(console_ctx_t *ctx, cmd_signature_t *reg)
{
  return xray_cmd_templ_single_int(ctx, reg,
                                   "x sn",
                                   "X-Ray Camera set number of frames (default 1)",
                                   "<int>", "Nb frames", 1,
                                   XRAY_PORT_DIRECT,
                                   XRAY_DETECTOR_SET_NFRAMES,
                                   "X-Ray Camera set number of frames error!",
                                   2000);
}

int xray_cmd_set_outputform(console_ctx_t *ctx, cmd_signature_t *reg)
{
  return xray_cmd_templ_single_int(ctx, reg,
                                   "x so",
                                   "X-Ray Camera set output form [0 to 63] (default 0)",
                                   "<int>", "Output Form", 0,
                                   XRAY_PORT_DIRECT,
                                   XRAY_SET_OUTPUTFORM,
                                   "X-Ray Camera set outputform error!",
                                   2000);
}

int xray_cmd_set_filtering(console_ctx_t *ctx, cmd_signature_t *reg)
{
  return xray_cmd_templ_single_int(ctx, reg,
                                   "x sf",
                                   "X-Ray Camera set filtering [1 = yes, 0 = no] (default 1)",
                                   "<int>", "Filtering", 1,
                                   XRAY_PORT_DIRECT,
                                   XRAY_SET_FILTERING,
                                   "X-Ray Camera set filtering error!",
                                   2000);
}

int xray_cmd_set_energy_hist_limit(console_ctx_t *ctx, cmd_signature_t *reg)
{
  return xray_cmd_templ_single_int(ctx, reg,
                                   "x sel",
                                   "X-Ray Camera set upper limit for energy histogram [0 = auto] (default 0)",
                                   "<int>", "Energy histogram limit", 0,
                                   XRAY_PORT_DIRECT,
                                   XRAY_SET_ENERGY_HISTOGRAM_LIMIT,
                                   "X-Ray Camera energy histogram limit error!",
                                   2000);
}

int xray_cmd_detector_measure(console_ctx_t *ctx, cmd_signature_t *reg)
{
   return xray_cmd_templ_no_arg(ctx, reg,
                               "x mn",
                               "X-Ray Measure, no turnoff [DK response]",
			       XRAY_PORT_DIRECT,
                               XRAY_DETECTOR_MEASURE,
                               "X-Ray measure error!",
                               2000);
}

int xray_cmd_detector_measure_scan(console_ctx_t *ctx, cmd_signature_t *reg)
{
   return xray_cmd_templ_no_arg(ctx, reg,
                               "x msn",
                               "X-Ray Measure, scanning mode, no turnoff [DK response]",
                               XRAY_PORT_DIRECT,
                               XRAY_DETECTOR_MEASURE_SCAN,
                               "X-Ray measure error!",
                               2000);
}

int xray_cmd_detector_measure_sd(console_ctx_t *ctx, cmd_signature_t *reg)
{
   return xray_cmd_templ_no_arg(ctx, reg,
                               "x m",
                               "X-Ray Measure [DK response]",
                               XRAY_PORT_DIRECT,
                               XRAY_DETECTOR_MEASURE_SHUTDOWN,
                               "X-Ray measure error!",
                               2000);
}

int xray_cmd_detector_measure_scan_sd(console_ctx_t *ctx, cmd_signature_t *reg)
{
   return xray_cmd_templ_no_arg(ctx, reg,
                               "x ms",
                               "X-Ray Measure, scanning mode [DK response]",
                               XRAY_PORT_DIRECT,
                               XRAY_DETECTOR_MEASURE_SCAN_SHUTDOWN,
                               "X-Ray measure error!",
                               2000);
}

int xray_cmd_save_config(console_ctx_t *ctx, cmd_signature_t *reg)
{
   return xray_cmd_templ_no_arg(ctx, reg,
                               "x svc",
                               "X-Ray save experiment Configuration",
                               XRAY_PORT_DIRECT,
                               XRAY_EXPERIMENT_SAVE_CONFIG,
                               "X-Ray configuration save error!",
                               5000);
}

int xray_cmd_reset_config(console_ctx_t *ctx, cmd_signature_t *reg)
{
   return xray_cmd_templ_no_arg(ctx, reg,
                               "x dlc",
                               "X-Ray delete experiment Configuration",
                               XRAY_PORT_DIRECT,
                               XRAY_EXPERIMENT_RESET_CONFIG,
                               "X-Ray configuration delete error!",
                               5000);
}

int xray_cmd_load_config(console_ctx_t *ctx, cmd_signature_t *reg)
{
   return xray_cmd_templ_no_arg(ctx, reg,
                               "x ldc",
                               "X-Ray load experiment Configuration",
                               XRAY_PORT_DIRECT,
                               XRAY_EXPERIMENT_LOAD_CONFIG,
                               "X-Ray load configuration error!",
                               5000);
}

int xray_cmd_sleep(console_ctx_t *ctx, cmd_signature_t *reg)
{
  return xray_cmd_templ_single_int(ctx, reg,
                                   "x sleep",
                                   "X-Ray Camera sleep (milliseconds) (default 1000)",
                                   "<int>", "Wait time (msec)", 1000,
                                   XRAY_PORT_DIRECT,
                                   XRAY_DETECTOR_SLEEP,
                                   "X-Ray Camera sleep command error!",
                                   2000);
}

int xray_cmd_get_status_dk(console_ctx_t *ctx, cmd_signature_t *reg)
{
   return xray_cmd_templ_no_arg(ctx, reg,
			       "x gsd",
                               "X-Ray Camera get Status [DK response]",
                               XRAY_PORT_DIRECT,
                               XRAY_GET_STATUS_DK,
                               "X-Ray Camera get status error!",
                               5000);
}

void register_cmd_xray()
{

  console_cmd_register(xray_cmd_create_storages,          "x cs"); // Create DK storages
  console_cmd_register(xray_cmd_reset_config,             "x dlc");
//console_cmd_register(xray_cmd_usb_power,                "x pwr"); 
  console_cmd_register(xray_cmd_detector_init,            "x in");
  console_cmd_register(xray_cmd_set_autoexec,             "x sax");
  console_cmd_register(xray_cmd_set_autoexec_dk,          "x saxdk");
  console_cmd_register(xray_cmd_system_shutdown,          "x sd");  
  console_cmd_register(xray_cmd_detector_set_acq_mode,    "x sm");
  console_cmd_register(xray_cmd_detector_set_bias,        "x sb");
  console_cmd_register(xray_cmd_save_config,              "x svc");
  console_cmd_register(xray_cmd_detector_set_threshold,   "x st");
  console_cmd_register(xray_cmd_detector_set_exposure,    "x se");
  console_cmd_register(xray_cmd_set_energy_hist_limit,    "x sel");
  console_cmd_register(xray_cmd_set_filtering,            "x sf");
  console_cmd_register(xray_cmd_sleep,                    "x sleep");
  console_cmd_register(xray_cmd_detector_set_nframes,     "x sn");
  console_cmd_register(xray_cmd_set_outputform,           "x so");
  console_cmd_register(xray_cmd_set_parameters,           "x sp");
  console_cmd_register(xray_cmd_detector_set_period,      "x spe");
  console_cmd_register(xray_cmd_set_roi,                  "x sr");
  console_cmd_register(xray_cmd_load_config,              "x ldc");
  console_cmd_register(xray_cmd_detector_measure_sd,      "x m");
  console_cmd_register(xray_cmd_detector_measure_scan_sd, "x ms");
  console_cmd_register(xray_cmd_detector_measure,         "x mn");
  console_cmd_register(xray_cmd_detector_measure_scan,    "x msn");
  console_cmd_register(xray_cmd_set_count,                "x spc");
  console_cmd_register(xray_cmd_get_metadata,             "x gm");
  console_cmd_register(xray_cmd_get_status,               "x gs");
  console_cmd_register(xray_cmd_get_status_dk,            "x gsd");
}
