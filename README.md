# vcom commands for RITE payload for vzlusat2

## Installation

### Step 1

Add the following code to `main.c`

```C
void register_cmd_xray(void);
register_cmd_xray();
```

### Step 2

Add the following code to `CMakeLists.txt`

```CMake
file(GLOB_RECURSE SOURCES
    "src/commands/xray/*.c"
)

target_include_directories(vcom
    PRIVATE "src/commands/xray"
)
```

## Commands

```
x -h

x cs
  Create DK storages
x dlc
  X-Ray delete experiment Configuration
x in
  X-Ray Initialize Timepix
x sax [<int>] [<int>]
  X-Ray set auto-execution (autorun, poweroff)
x saxdk [<int>] [<int>]
  X-Ray set auto-execution (autorun, poweroff) [via DK]
x sd
  X-Ray Shut down Mars control computer
x sm [<int>]
  X-Ray Camera set aquisition mode (0 = MPX, 1 = TOT, 2 = TPX)
x sb [<real>]
  X-Ray Camera set bias (V) (default 50.0)
x svc
  X-Ray save experiment Configuration
x st [<real>]
  X-Ray Camera set energy threshold (keV) (default 5.0)
x se [<int>]
  X-Ray Camera set exposure time (ms) (default 100)
x sel [<int>]
  X-Ray Camera set upper limit for energy histogram [0 = auto] (default 0)
x sf [<int>]
  X-Ray Camera set filtering [1 = yes, 0 = no] (default 1)
x sleep [<int>]
  X-Ray Camera sleep (milliseconds) (default 1000)
x sn [<int>]
  X-Ray Camera set number of frames (default 1)
x so [<int>]
  X-Ray Camera set output form [0 to 63] (default 0)
x sp [<int>] [<dbl>] [<int>] [<int>] [<int>] [<int>] [<int>] [<int>] [<int>] [<int>]
  X-Ray set parameters (mode, threshold, exposure, frames, filtering,
  outputform, scan, count-threshold, count-roi, en-hist-limit)
x spe [<int>]
  X-Ray Camera set frame period (ms) (default 0)
x sr [<int>] [<int>] [<int>] [<int>] [<int>]
  X-Ray Camera Set ROI (id, row, col, height, width)
x ldc
  X-Ray load experiment Configuration
x m
  X-Ray Measure [DK response]
x ms
  X-Ray Measure, scanning mode [DK response]
x mn
  X-Ray Measure, no turnoff [DK response]
x msn
  X-Ray Measure, scanning mode, no turnoff [DK response]
x spc [<int>] [<int>]
  X-Ray set Count treshold (treshold, id)
x gm [<int>]
  Get image metadata [fetch DK]
x gs
  X-Ray get status
x gsd
  X-Ray Camera get Status [DK response]
```

## Example

```
x gm

[RITE] start_nb: 2
[MEAS] exp_nb: 0
[MEAS] frame_id: 0x0200
[MEAS] time: 2020-08-19 17:05:58.571
[DATA] outputForm: 63
[DATA] firstChunkId: 1161
[DATA] lastChunkId: 1275
[DET] mode: 0
[DET] scan mode, (count) threshold, roi: 0, 500, 0
[DET] frameTime: 10 (sec)
[DET] bias: 50 (V)
[DET] threshold: 2.49568 (keV)
[DET] nFrames: 1 
[PROC] filtering: 1 
[PROC] (frame) min, max, (filtered) min, max:          0         23          0         23
[PROC]    id  row  col  hgt  wdh  frame-evt   filt-evt  frame-sum   filt-sum
[PROC] roi0:    0    0  256  256       2243        424       2619        738
[PROC] roi1:    0    0  128  128        678        141        813        255
[PROC] roi2:    0  128  128  128        641        176        765        269
[PROC] roi3:  128    0  128  128        511         58        579        119
[PROC] roi4:  128  128  128  128        413         49        462         95
```
