/**

 * XRAY
 *
 * @author zmatej@centrum.cz
 */

#ifndef _XRAY_H_
#define _XRAY_H_

#include <stdint.h>

#define NODE_XRAY		     16
#define NODE_OBC                     1

#define PORT_DK                      12

#define DET_DEV_IDX                  0
#define DET_FRAME_SZ                 65536
#define DET_HEIGHT                   256
#define DET_WIDTH                    256
#define DET_NROIS                    5
#define DET_ENERGY_NBINS             16

#define XRAY_DEFAULT_BIAS            200.0

#define SCAN_MODE_SIMPLE            0
#define SCAN_MODE_COUNTING          1

/* The ports from 8 to 47 are used for subsystem specific services. */
#define XRAY_PORT_DIRECT            15
#define XRAY_PORT_DK                16
#define XRAY_PORT_DK_ACK            17
#define XRAY_PORT_DK_COM_ACK        18
#define XRAY_PORT_DK_INFO_ACK       21

/* DK storage */
#define XRAY_MAX_CHUNK_SIZE       200
#define XRAY_PORT_DK_PIC_RAW       70
#define XRAY_PORT_DK_PIC_INFO      71

#define XRAY_REPLY_BUF_SZ         100
#define XRAY_DK_SLEEP_TIME_MS      20

#define XRAY_OUTPUTFORM_BIN_1    0x01
#define XRAY_OUTPUTFORM_BIN_8 	 0x02
#define XRAY_OUTPUTFORM_BIN_16 	 0x04
#define XRAY_OUTPUTFORM_BIN_32 	 0x08
#define XRAY_OUTPUTFORM_HIST     0x10
#define XRAY_OUTPUTFORM_ENHIST   0x20
#define XRAY_OUTPUTFORM_FRAME    0x40
#define XRAY_OUTPUTFORM_FRAME_MASKED     0x80
#define XRAY_OUTPUTFORM_SINGLE_ROI       0x100
#define XRAY_OUTPUTFORM_FRAME_MASKED2    0x200

typedef enum {
        STORAGE_HK_ID                           = 1,
        STORAGE_METADATA_ID                     = 2,
        STORAGE_DATA_ID                         = 3,
} DK_STORAGES_IDS;

typedef enum
{
  XRAY_CAMERA_CONF = 0x10,
  XRAY_TEST,                  // test
  XRAY_DK_CREATE_STORAGES,    // create all storages in the data keeper
  XRAY_USB_POWER,
  XRAY_DETECTOR_INIT,
  XRAY_SYSTEM_SHUTDOWN,
  XRAY_DETECTOR_SET_ACQ_MODE,
  XRAY_DETECTOR_SET_BIAS,
  XRAY_DETECTOR_SET_THRESHOLD,
  XRAY_DETECTOR_SET_EXPOSURE,
  XRAY_DETECTOR_SET_NFRAMES,
  XRAY_DETECTOR_SET_ROI,
  XRAY_SET_OUTPUTFORM,
  XRAY_SET_FILTERING,
  XRAY_SET_ENERGY_HISTOGRAM_LIMIT,
  XRAY_SET_COUNT_TRESHOLD,
  XRAY_DETECTOR_MEASURE,
  XRAY_DETECTOR_MEASURE_SCAN,
  XRAY_DETECTOR_MEASURE_SHUTDOWN,
  XRAY_DETECTOR_MEASURE_SCAN_SHUTDOWN,
  XRAY_DETECTOR_SET_PARAMETERS,
  XRAY_EXPERIMENT_SAVE_CONFIG,
  XRAY_EXPERIMENT_RESET_CONFIG,
  XRAY_EXPERIMENT_LOAD_CONFIG,
  XRAY_SET_AUTOEXEC,
  XRAY_DETECTOR_SET_PERIOD,
  XRAY_GET_STATUS,
  XRAY_GET_STATUS_DK,
  XRAY_DETECTOR_SLEEP,
} xray_cmd_list_t;

typedef struct __attribute__((packed))
{
  uint8_t cmd;
} xray_cmd_t;

typedef struct __attribute__((packed))
{
  xray_cmd_t parent;
  int32_t arg0;
} xray_cmd_single_int_t;

typedef struct __attribute__((packed))
{
  xray_cmd_t parent;
  double arg0;
} xray_cmd_single_dbl_t;

typedef struct __attribute__((packed))
{
  xray_cmd_t parent;
  int id;
  int row;
  int col;
  int height;
  int width;
} xray_cmd_setroi_t;

typedef struct __attribute__((packed))
{
  xray_cmd_t parent;
  uint32_t treshold;
  uint8_t id;
} xray_cmd_setcount_t;

typedef struct __attribute__((packed))
{
  xray_cmd_t parent;
  uint8_t autoexec;
  uint8_t poweroff;
} xray_cmd_autoexec_t;

typedef struct __attribute__((packed))
{
  xray_cmd_t parent;
  uint32_t ntest;
} xray_cmd_test_t;

typedef enum
{
  XRAY_OK = 0,
  XRAY_CAMERA_CONFIG_DEVICE_ERROR,
  XRAY_CAMERA_DK_CONF_CHUNK_ERROR,
  XRAY_CAMERA_UNSPECIFIED_ERROR = 0xff,
} xray_reply_ack_list_t;

typedef struct __attribute__((packed))
{
  uint8_t ack;
} xray_reply_t;

typedef struct __attribute__((packed))
{
  xray_cmd_t parent;
  uint8_t    mode;
  double     threshold;
  uint32_t   frameTime;
  uint16_t   nFrames;
  uint8_t    filtering;
  uint16_t   outputform;
  uint8_t    scanMode;
  uint32_t   countThreshold;
  uint8_t    countROI;
  uint16_t   en_hist_limit;
} xray_cmd_setparam_t;

typedef struct __attribute__((packed))
{
  uint8_t    packetType;
  uint8_t    valid;
  uint8_t    automeasure;
  uint8_t    turnoff;
  uint8_t    mode;
  uint8_t    filtering;
  uint8_t    scanMode;
  uint8_t    countROI;
  double     threshold;
  double     bias;
  double     frameTime;
  double     framePeriod;
  uint16_t   nFrames;
  uint16_t   outputform;
  uint32_t   countThreshold;
  uint16_t   en_hist_limit;
  uint16_t   rois[DET_NROIS][4];
} experiment_config_t;

typedef struct __attribute__((packed))
{
  uint8_t packetType;
  uint8_t status;
  uint32_t tv_sec;
  uint32_t tv_nsec;
} xray_status_t;

typedef struct __attribute__((packed))
{
  uint8_t packetType;
  uint16_t outputForm;
  // application & experimet
  uint16_t start_nb;
  uint16_t exp_nb;
  // detector
  uint8_t mode; // alligned -- 4 bytes ---
  double frameTime;
  double bias;
  double threshold;
  uint16_t en_hist_limit;
  uint16_t rois[DET_NROIS][4];
  uint16_t nFrames;
  // processing
  uint16_t frame_id;
  uint8_t filtering;
  uint8_t scanMode;
  uint8_t count_roi;
  uint32_t count_threshold;
  //uint16_t en_hist_threshold;
  uint16_t minValueOriginal;
  uint16_t maxValueOriginal;
  uint16_t minValueFiltered;
  uint16_t maxValueFiltered;
  uint16_t frame_events[DET_NROIS];
  uint16_t frame_events_filtered[DET_NROIS];
  uint32_t roi_sum[DET_NROIS];
  uint32_t roi_sum_filtered[DET_NROIS];
  // info
  uint32_t firstChunkId;
  uint32_t lastChunkId;
  // position from ADCS
  //int16_t attitude[7];	
  // position from ADCS
  //int16_t position[3];
  // position from OBC?
  //uint32_t position2[2];
  uint32_t tv_sec;
  uint32_t tv_nsec;
  //uint32_t tv_rms;
} experiment_metainfo_t;

/* Common task for all WorkerThreads */
typedef enum {
    TASK_VOID = 0,
} xray_common_task_t;

#endif /* _XRAY_H_ */
